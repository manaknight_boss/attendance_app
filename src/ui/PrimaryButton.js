import { Button } from 'react-native-paper';
import React from 'react';
import styled from 'styled-components/native'


function PrimaryButton(props) {
  return (
    <StyledButton 
        labelStyle={{color:"white"}}
        mode="contained"
        {...props} 
    />
  )
}
export default PrimaryButton

const StyledButton = styled(Button)`
padding: 10px 0;
margin: 25px 0;
border-radius: 30px;
${({secondary}) => secondary && 'background-color: black;'}
`
