import React from 'react'
import styled from 'styled-components'
import { ActivityIndicator } from 'react-native-paper';

function Loading(props){
  return (
    <Backdrop {...props}>
    	<ActivityIndicator size='large' color='orange' animating={true} />
    </Backdrop>
  );
}

export default Loading

const Backdrop = styled.View`
position: absolute;
top: 0;
left:0;
width: 100%;
height: 100%;

justify-content: center;
align-items: center;

background-color: white;
`;
