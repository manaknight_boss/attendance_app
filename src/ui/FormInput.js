import { TextInput } from 'react-native-paper';
import styled from 'styled-components/native'

const FormInput = styled(TextInput)`
margin: 10px 0;
background-color: white;
`

export default FormInput
