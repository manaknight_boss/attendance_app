import { Appbar } from 'react-native-paper';
import React from 'react';

function Header(props) {

  const { 
    title,
    subtitle, 
    navigation,
    back,
  } = props

  return (
    <Appbar.Header>
      <Appbar.Action 
        color="white" 
        icon={back ? "arrow-left" : "menu"}
        onPress={back ? () => navigation.goBack() : navigation.openDrawer} />

      <Appbar.Content
        title={title || "Title"}
        subtitle={subtitle}
        titleStyle={{color:'white'}}
      />
    </Appbar.Header>
  )
}
export default Header
