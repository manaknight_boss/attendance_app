import {
  Avatar,
  Divider,
  Drawer,
  Headline,
  Menu,
  Text
} from 'react-native-paper';
import { SafeAreaView, ScrollView, StyleSheet, View } from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import React from 'react';
import styled from 'styled-components/native'
import {connect} from 'react-redux'

function CustomDrawer(props) {

  const { 
    items, 
    activeItemKey,
    onItemPress,
    renderIcon,
    getLabel,
    activeTintColor,
    inactiveTintColor,
    iconContainerStyle,
    navigation,
    profile
  } = props;

  return (
    <>
      <ScrollView>
        <SafeAreaView
          style={S.container}
          forceInset={{ top: 'always', horizontal: 'never' }}
        >

          <Drawer.Section style={S.drawerHeader}>
            <HeaderContent>
              <Headline>{profile.first_name} {profile.last_name}</Headline>
              <MenuAnchor label={profile.table} />
            </HeaderContent>
          </Drawer.Section>

          {items.map( (route, index) => {

            const tintColor = focused ? activeTintColor : inactiveTintColor;
            const focused = route.key === activeItemKey
            const scene = { route, index, focused, tintColor };
            const icon = renderIcon(scene);
            const label = getLabel(scene);

            return (
              <Drawer.Item 
                key={index} 
                label={label} 
                icon={() => <IconContainer icon={icon} style={iconContainerStyle} />}
                active={focused}
                onPress={() => onItemPress({route, focused})}
                style={{borderRadius: 30 }}
              />
            )
          })}
        </SafeAreaView>
      </ScrollView>
      <View>
        <Drawer.Item 
          label="Salir" 
          icon={() => <Icon name="logout" size={24} style={{marginLeft: 5}} />}
          onPress={() => navigation.navigate('Login') }
          style={{borderRadius: 30, marginVertical: 15 }}
        />
      </View>
    </>
  )
}
export default connect(
    ({profile}) => ({profile})
)(CustomDrawer)

const IconContainer = ({icon, ...props}) => {
  return (
    <View {...props}>
      {icon}
    </View>
  )
}

const MenuAnchor = ({label}) => {
  const [visible, setVisible] = React.useState(false)

  const openMenu = () => {
    setVisible(true)
  }

  const closeMenu = () => {
    setVisible(false)
  }

  return (
    <Menu
      visible={visible}
      onDismiss={closeMenu}
      anchor={
        <TouchableOpacity onPress={openMenu}>
          <View style={S.menuAnchor}>
            <Text>{label}</Text>
            <Icon name="menu-down" size={24} />
          </View>
        </TouchableOpacity>
      }
    >
      <Menu.Item onPress={() => {}} title="Item 1" />
      <Menu.Item onPress={() => {}} title="Item 2" />
      <Divider />
      <Menu.Item onPress={() => {}} title="Item 3" />
    </Menu>
  )
}

const HeaderContent = styled.View`
padding: 0 15px;
`
const S = StyleSheet.create({
  drawerHeader: {
    paddingTop: 50,
  },
  container: {
    flex: 1
  },
  avatar: {
    marginVertical: 10
  },
  menuAnchor: {
    flexDirection: 'row'
    , alignItems: 'center'
    , paddingVertical: 5
    , width: 100
  }
});
