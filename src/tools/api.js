import axios from 'axios'
import store from '../redux/store'

const URL = 'https://avanza360.app/v1/api'

export const client = (bt) => {
    const { bearerToken } =  store.getState()

    const instance =  axios.create({
        baseURL: URL,
        headers: {
            'content-type': 'application/json'
        }
    })

    if (!bt) bt = bearerToken;
    if (bt) instance.defaults.headers.common['Authorization'] = bt ;

    return instance
}

export async function login(email, password) {
    console.log('API login ->', email, '~', password )
    const {data}= await client().post(`/grassroot/login`, { email, password })
    const {token, expires_in: tokenExpiration} = data.access_token
    console.log('Token acquired', '\n');
    return {token, tokenExpiration}
}

export async function addTurns(turns, table) {
    console.log('API addTurns -> ', 'Table:', table, 'Turns:', turns )
    const {data} = await client().post(`/voted/municipal/${table}`, { voter_turns: turns })

    if (data.success && data.voters) console.log("Turns added successfully", '\n')

    return true

}

export async function getProfile() {
    console.log("API getProfile", )
    const {data} = await client().get('/grassroot/profile')
    const { data: profile } = data
    console.log('Profile retrieved', '\n')
    return profile
}

export async function searchRecinto(govId) {
    console.log('API searchRecinto ->', 'Gov ID:', govId)
    const {data} = await client().get(`/government/${govId}`)
    const {data:recinto} = data;
    console.log('Recinto Found:', recinto.recinto_name, '\n')
    return recinto
}

export async function getTableResults(table) {
    console.log('API getTableResults ->', 'Table:', table)
    const {data} = await client().get(`/table/results/municipal/${table}`)
    const {data: results} = data
    console.log('Results retrieved', '\n')
    return results
}

export async function getVotersByCoordinator(coordinatorId, table) {
    console.log('API getVotersByCoordnator -> ', 'Coordinator:', coordinatorId, 'Table:', table)
    const {data} = await client().get(`/table/cordinator/municipal/${coordinatorId}/${table}`)
    const {data: result} = data
    const {people} = result;
    console.log('Results retrieved', '\n')
    return people;
}
