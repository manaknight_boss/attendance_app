import {createDrawerNavigator} from 'react-navigation-drawer';

import CustomDrawer from '../ui/CustomDrawer';
import DashboardStack from './stacks/DashboardStack';
import HomeScreen from '../screens/HomeScreen';
import LocatorScreen from '../screens/LocatorScreen';
import FinalCount from '../screens/FinalCount'

const DrawerNavigator = createDrawerNavigator({
  Home: HomeScreen,
  Dashboard: { screen: DashboardStack, params: { icon: 'view-dashboard' } },
  Locator: { screen: LocatorScreen, params: { icon: 'database-search' } },
  FinalCount: { screen: FinalCount },
}, {
  initialRouteName: 'Home',
  contentComponent: CustomDrawer,
  contentOptions: {
    iconContainerStyle: {
      marginLeft: 5
    }
  }
})

export default DrawerNavigator
