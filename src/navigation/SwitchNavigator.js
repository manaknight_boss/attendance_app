import { createSwitchNavigator } from 'react-navigation';

import DrawerNavigator from './DrawerNavigator';
import LoginScreen from '../screens/auth/LoginScreen';

const SwitchNavigator = createSwitchNavigator({
  Login: LoginScreen,
  App: DrawerNavigator
}, {
  initialRouteName: 'Login'
})

export default SwitchNavigator
