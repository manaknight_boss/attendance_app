import {createStackNavigator} from 'react-navigation-stack';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import React from 'react';

import CoordinatorScreen from '../../screens/CoordinatorScreen';
import DashboardScreen from '../../screens/DashboardScreen';

const DashboardStack = createStackNavigator({
  DashboardScreen,
  CoordinatorScreen
}, {
  initialRouteName: 'DashboardScreen',
  headerMode: 'none',
  navigationOptions: {
    drawerLabel: "Resumen",
    drawerIcon: (focused, tintColor) => <Icon name="clipboard-text-outline" size={24} color={tintColor} />
  }
})
export default DashboardStack
