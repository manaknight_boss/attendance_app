import { createAppContainer } from 'react-navigation';

import SwitchNavigator from './SwitchNavigator';

const NavigationContainer = createAppContainer(SwitchNavigator)
export default NavigationContainer

