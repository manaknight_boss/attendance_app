import {Linking, Platform,} from 'react-native'
import { Text, Button, Headline, Subheading } from 'react-native-paper';
import { FlatList, ScrollView, View } from 'react-native';
import React from 'react';
import styled from 'styled-components/native'
import {connect} from 'react-redux'
import {getVotersByCoordinator} from '../tools/api'
import Loading from '../ui/Loading'

import {
    Label,
    OverviewRow,
    Stats,
    TableHeader,
    TableHeaderText,
    Value,
} from './DashboardScreen';
import { Row, Col, Table, TableHead } from '../ui/tabular';
import Header from '../ui/Header'

function CoordinatorScreen(props) {
    const {profile, navigation} = props
    const coordinator = navigation.getParam('coordinator')
    const [voters, setVoters] = React.useState([])
    const [loading, setLoading] = React.useState(true);

    const getVoters = async () => {
      try{
        const v = await getVotersByCoordinator(coordinator.id, profile.table)
        setVoters(v)
      }
      catch(error) {
	consle.error(error.message)
      }
      finally {
	setLoading(false)
      }

    }

    const makeCall = async (number) => {
        console.log('Call', number)
        if (Platform.OS === 'android') {
            phoneNumber = `tel:${number}`;
        } else {
            phoneNumber = `telprompt:${number}`;
        }

        Linking.openURL(phoneNumber);
    }

    const renderItem = ({item, index, separators}) => {
      const {phone_1, phone_2} = item
      const phone = phone_1 || phone_2
	return (
	  <Row odd={index%2===0}>
	      <Col><Text>{item.first_name} {item.last_name}</Text></Col>
	      <Col centered><Text>{phone_1 || phone_2 }</Text></Col>
	      <Col centered>
	  	{phone ? 
		  <Button
		      icon="phone-in-talk"
		      onPress={() => makeCall(phone)}>
		      Llamar
		  </Button> 
		  : null}
	      </Col>
	  </Row>
	)
    }

    React.useEffect(() => {
        getVoters()
    }, [])

    const phone = coordinator.phone_1 || coordinator.phone_2

    return (
        <>
            <Header title="Coordinador" navigation={navigation} back />

            <Coordinator>
                <Headline style={{textAlign: 'center'}}>
                    {coordinator.first_name} {coordinator.last_name}
                </Headline>

      		{phone ? <Button
		  icon="phone-in-talk"
		  onPress={() => makeCall( phone )} >
                    {phone}
                </Button> : null}

            </Coordinator>

            <Stats>
                <OverviewRow>
                    <Label>Suscritos</Label>
                    <Value>{coordinator.total}</Value>
                </OverviewRow>
                <OverviewRow>
                    <Label>Por Votar</Label>
                    <Value>{coordinator.left}</Value>
                </OverviewRow>
            </Stats>

            <TableHeader>
                <TableHeaderText>
                    POR VOTAR
                </TableHeaderText>
            </TableHeader>

            <TableHead headings={['Votante', 'Tel\u00e9fono', 'Acci\u00f3n']} />
	    <FlatList
	      data={voters}
	      keyExtractor={item => item.phone_1}
	      renderItem={renderItem} 
	      removeClippedSubviews={true}
	      initialNumToRender={7}
	    />
      		
           {loading && <Loading />}
        </>
    )
}

export default connect(
    ({profile}) => ({profile})
)(CoordinatorScreen)

const Coordinator = styled.View`
text-align: center;
justify-content: center;
align-items: center;
margin: 15px 0 0;
`
/*
            <View style={{flex:1}}>
                <ScrollView>
                    {voters.map((voter, r) => {
                        return (
                            <Row odd={r%2 === 0} key={r}>
                                <Col centered style={{justifyContent:'center'}}><Text>{voter.first_name} {voter.last_name}</Text></Col>
                                <Col centered><Text>{voter.phone_1}</Text></Col>
                                <Col centered>
                                    <Button
                                        icon="phone-in-talk"
                                        onPress={() => makeCall(voter.phone_1)}>
                                        Llamar
                                    </Button> 
                                </Col>
                            </Row>
                        )
                    })}
                </ScrollView>
            </View>
 */
