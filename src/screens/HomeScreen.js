import {View} from 'react-native'
import { Button, Headline, List } from 'react-native-paper';
import React from 'react';
import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Content from '../ui/Content';
import FormInput from '../ui/FormInput';
import Header from '../ui/Header';
import PrimaryButton from '../ui/PrimaryButton';
import {addTurns} from '../tools/api'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import Loading from '../ui/Loading'

function HomeScreen(props) 
{
  const { profile, navigation } = props
  const [numbers, setNumbers] = React.useState(Array(5).fill(null))
  const [loading, setLoading] = React.useState(false)

  const addNumber = () => {
    const tmpNumbers = [...numbers]
    tmpNumbers.push("")
    setNumbers(tmpNumbers)
  }

  const setNumberValue = (i, value) => {
    const tmp = [...numbers]
    tmp[i] = value
    setNumbers(tmp)
  }

    const send = async () => {
	setLoading(true)
        try {
            await addTurns(numbers, profile.table)
	    setNumbers(Array(5).fill(null))
        }
        catch(error) {
            console.error(error)
        }
      	finally {
	  setLoading(false)
	}
    }

  return (
    <>
      <Header title="Ingresar Turnos" navigation={navigation}/>
      {loading ? <Loading /> : (
      <View style={{flex:1}}>
	<Content>
	  {numbers.map((n,i) => {
	    const label = `Turno ${i+1}`
	    return (
	      <FormInput
		key={i}
		label={label}
		value={n}
		onChangeText={value => setNumberValue(i, value)}
		mode="outlined"
	      />
	    )
	  })}
	</Content>

	<SideMargin>
	  <PrimaryButton secondary onPress={send} > Enviar </PrimaryButton>
	</SideMargin>
	</View>
      )}
    </>
  )
}

const HomeContainer = connect(
    ({profile}) => ({profile}),
)(HomeScreen)

HomeContainer.navigationOptions = {
  drawerLabel: 'Agregar Turnos',
  drawerIcon: (focused, tintColor) => <Icon name="textbox" size={24} color={tintColor} />,
}
export default HomeContainer;

const StyledHeadline = styled(Headline)`
font-weight: bold;
margin: 15px 0;
`

const ButtonNew = styled(Button)`
margin: 20px 0;
`

const SideMargin = styled.View`
margin: 0 25px;
`
