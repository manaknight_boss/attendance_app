import axios from 'axios'
import { Headline, Subheading } from 'react-native-paper';
import { StyleSheet } from 'react-native';
import React from 'react';
import styled from 'styled-components/native'

import Content from '../../ui/Content';
import FormInput from '../../ui/FormInput';
import PrimaryButton from '../../ui/PrimaryButton';
import { connect } from 'react-redux'
import {  bindActionCreators} from 'redux'

import { login as apiLogin, getProfile } from '../../tools/api'
import { setBearerToken, setProfile } from '../../redux/actions'
import store from '../../redux/store'
import { client }  from '../../tools/api'
import Loading from '../../ui/Loading'

function LoginScreen(props) {
    const {
        email, 
        password, 
        navigation,
        setBearerToken,
        setProfile
    } = props

    const [loginEmail, setLoginEmail] = React.useState("")
    const [loginPassword, setLoginPassword] = React.useState("")
    const [loading, setLoading] = React.useState(false)

    const login = async () => {
      setLoading(true)
        try {
            const { token } = await apiLogin(email, password)
            if(token) {
                setBearerToken( `Bearer ${token}` );
                const profile = await getProfile()
                if(profile) {
                    setProfile(profile)
                    navigation.navigate('App')
                }
            }
        }

        catch(e) {
            console.error(e.message)
            console.log(e.response.data)
            console.log(e.response)
	    setLoading(false)
        }
    }


  return (
    <>
    <Content contentContainerStyle={S.content}>
      <Headline>Bienvenido(a)</Headline>
      <Subheading style={S.gray}>Accede para continuar</Subheading>

      <Form>
        <FormInput
          label="Email"
            value={email}
            onChangeText={setLoginEmail}
        />
        <FormInput
          label="Contrase&ntilde;a"
            value={password}
            onChangeText={setLoginPassword}
        />
      </Form>
      <PrimaryButton 
        labelStyle={{color: 'white'}}
        mode="contained"
        onPress={login} >
        Acceder
      </PrimaryButton>
    </Content>
    {loading && <Loading />}
    </>
  )
}

export default connect(
    ({email, password}) => ({email, password}),
    dispatch => bindActionCreators({
        setBearerToken,
        setProfile
    }, dispatch)
)(LoginScreen)

const Form = styled.View`
margin: 30px 0;
`

const S = StyleSheet.create(
  {
    gray: {
      color: '#C4C4C4',
    },
    content: {
      flex: 1,
      justifyContent: 'center', 
    }
  }
)
