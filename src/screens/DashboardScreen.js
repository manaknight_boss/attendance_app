import { Button, Text } from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';
import { StyleSheet, FlatList, View } from 'react-native';

import React from 'react';
import styled from 'styled-components/native'

import { Row, Col, Table, TableHead } from '../ui/tabular';
import { theme } from '../theme';
import Header from '../ui/Header';
import {getTableResults} from '../tools/api'
import {connect} from 'react-redux'
import Loading from '../ui/Loading'

function DashboardScreen(props) {
    const {profile, navigation} = props
    const [coordinators, setCoordinators] = React.useState([])
    const [voters, setVoters] = React.useState(0)
    const [voted, setVoted] = React.useState(0)
    const [notVoted, setNotVoted] = React.useState(0)
    const [loading, setLoading] = React.useState(true);

    const getData = async () => {
      	try {
	  const results = await getTableResults(profile.table)
	  setCoordinators(results.cordinators)
	  setVoters(results.total_voters)
	  setVoted(results.total_voted)
	  setNotVoted(results.total_not_voted)
	}
      	catch(error) {
	  console.error(error.message)
	  console.error(error.response.data)
	}
      	finally {
	  setLoading(false)
	}
    }

    const renderItem = ({item, index, separators}) => {
	return (
	  <Row odd={index%2===0}>
	      <Col><Text>{item.first_name} {item.last_name}</Text></Col>
	      <Col centered><Text>{item.left}</Text></Col>
	      <Col centered>
		  <Button 
		      icon="magnify"
		      onPress={() => navigation.push("CoordinatorScreen", {coordinator:item})}
		  >
		      Ver
		  </Button> 
	      </Col>
	  </Row>
	)
    }

    React.useEffect(() => {
        getData()
    }, [])

    return (
        <>
            <Header title="Resumen" navigation={navigation} />

            <Stats>
                <OverviewRow>
                    <Label>Votantes</Label>
                    <Value>{voters}</Value>
                </OverviewRow>
                <OverviewRow>
                    <Label>Votaron</Label>
                    <Value>{voted}</Value>
                </OverviewRow>
                <OverviewRow>
                    <Label>Por Votar</Label>
                    <Value>{notVoted}</Value>
                </OverviewRow>
            </Stats>

            <TableHeader>
                <TableHeaderText>
                    SIN VOTAR (POR COORDINADOR)
                </TableHeaderText>
            </TableHeader>

            <TableHead headings={['Coordinador', 'Votantes', 'Acci\u00f3n']} />
	    <FlatList
	      data={coordinators.slice(1,coordinators.length -1)}
	      keyExtractor={item => item.id}
	      renderItem={renderItem} 
	      removeClippedSubviews={true}
	      initialNumToRender={7}
	      maxToRenderPerBatch={7}
	      updateCellsBatchingPeriod={1000}
	    />
      		
           {loading && <Loading />}
        </>
    )
}

export default connect(
    ({profile}) => ({profile})
)(DashboardScreen)


export const Label = styled(({children, ...props}) => {
    return (
        <View {...props}>
            <LabelText>{children}</LabelText>
        </View>
    )
})`
flex: 1;
align-items: flex-end;
padding-right: 5px;
`
export const Value = styled(({children, ...props}) => {
    return (
        <View {...props}>
            <ValueText>{children}</ValueText>
        </View>
    )
})`
flex: 1;
align-items: flex-start;
padding-left: 5px;
`

export const Stats = styled.View`
padding: 15px 0;
`


export const LabelText = styled(Text)`
font-size: 16px;
`

export const ValueText = styled(Text)`
font-size: 25px;
color: ${theme.colors.primary};
`

export const TableHeader = styled.View`
align-items: center;
background-color: #F3F3F3;
padding: 13px 0;
`

export const TableHeaderText = styled(Text)`
font-size: 16px;
`

export const OverviewRow = styled.View`
flex-direction: row;
align-items: center;

`

const S = StyleSheet.create({
  firstCol: {

  }
})


/*
<View style={{flex:1}}>
    <ScrollView>
	{coordinators.map((coordinator, r) => {
	    const length = coordinators.length
	    const isLast = i => i % length  === length -1 && !Action
	    return (
		<Row odd={r%2 === 0} key={coordinator.id}>
		    <Col style={{justifyContent:'center'}}><Text>{coordinator.first_name} {coordinator.last_name}</Text></Col>
		    <Col centered><Text>{coordinator.left}</Text></Col>
		    <Col centered>
			<Button 
			    icon="magnify"
			    onPress={() => navigation.push("CoordinatorScreen", {coordinator})}
			>
			    Ver
			</Button> 
		    </Col>
		</Row>
	    )
	})}
    </ScrollView>
</View>
 */
