import { Headline, Subheading, Text, Searchbar } from 'react-native-paper';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import React from 'react';
import styled from 'styled-components/native'

import { theme } from '../theme';
import Content from '../ui/Content';
import FormInput from '../ui/FormInput';
import Header from '../ui/Header'
import PrimaryButton from '../ui/PrimaryButton';
import {searchRecinto} from '../tools/api'

function LocatorScreen({navigation}) {
    const [id, setId] = React.useState("00114534696")
    const [recinto, setRecinto] = React.useState({})

    const search = async () => {
        const rec = await searchRecinto(id)
        setRecinto(rec)
    }

    return (
        <>
            <Header title="Buscar Recintos" navigation={navigation} />
            <Content>

                <Searchbar
                    placeholder='Ingresar C&eacute;dula'
                    style={S.search}
	            value={id}
                    onChangeText={(v) => setId(v)}
                />

                <Results>
                    <Result>
                        <Subheading>Colegio</Subheading>
                        <ResultText>{recinto.colegio}</ResultText>
                    </Result>

                    <Result>
                        <Subheading>Recinto</Subheading>
                        <ResultText>{recinto.recinto_name}</ResultText>
                    </Result>
                </Results>
            </Content>

            <SideMargin>
                <PrimaryButton secondary onPress={search}> Buscar </PrimaryButton>
            </SideMargin>
        </>
    )
}
export default LocatorScreen

LocatorScreen.navigationOptions = {
    drawerLabel: "Buscar Recintos",
    drawerIcon: (focused, tintColor) => <Icon name="database-search" size={24} color={tintColor} />
}

const ResultText = styled(Text)`
color: ${theme.colors.primary}
font-weight: bold;
font-size: 25px;
`
const Results = styled.View`
flex-direction: row;
margin: 25px 0;
`
const Result = styled.View`
margin-right: 30px;
`
const SideMargin = styled.View`
margin: 0 25px;
`

const S = StyleSheet.create({
    search: {
        marginVertical: 25
    }
})
