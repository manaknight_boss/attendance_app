import { Text } from 'react-native-paper';
import { View } from 'react-native';
import React from 'react';
import styled from 'styled-components/native'

import { Table, TableHead } from '../ui/tabular';
import { theme } from '../theme';
import Header from '../ui/Header';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

function FinalCount({navigation}) {
  const alcaldes = [ 
    ['Juan Perez', '234'], ['Juan James', '987'] ,
    ['Juan Perez', '234'], ['Juan James', '987'] ,
  ]

  const regidores = [ 
    ['Juan Perez', '234'], ['Juan James', '987'] ,
    ['Juan Perez', '234'], ['Juan James', '987'] ,
  ]

  return (
    <>
      <Header title="Cuenta Final" navigation={navigation} />

      <Stats>
        <Row>
          <Label>Mesa #</Label>
          <Value>1111</Value>
        </Row>
        <Row>
          <Label>Ticket #</Label>
          <Value>666</Value>
        </Row>
      </Stats>

      <TableHeader>
        <TableHeaderText>
          ALCALDES
        </TableHeaderText>
      </TableHeader>

      <TableHead headings={['Nombre', 'Votos',]} />
      <Table data={alcaldes} />
      <TableHeader>
        <TableHeaderText>
          REGIDORES
        </TableHeaderText>
      </TableHeader>

      <TableHead headings={['Nombre', 'Votos',]} />
      <Table data={regidores} />
    </>
  )
}
export default FinalCount

FinalCount.navigationOptions = {
  drawerLabel: 'Cuenta Final',
  drawerIcon: (focused, tintColor) => <Icon name="counter" size={24} color={tintColor} />,
}

export const Label = styled(({children, ...props}) => {
  return (
    <View {...props}>
      <LabelText>{children}</LabelText>
    </View>
  )
})`
flex: 1;
align-items: flex-end;
padding-right: 5px;
`
export const Value = styled(({children, ...props}) => {
  return (
    <View {...props}>
      <ValueText>{children}</ValueText>
    </View>
  )
})`
flex: 1;
align-items: flex-start;
padding-left: 5px;
`

export const Stats = styled.View`
padding: 15px 0;
`

export const Row = styled.View`
flex-direction: row;
align-items: center;
`

export const LabelText = styled(Text)`
font-size: 16px;
`

export const ValueText = styled(Text)`
font-size: 25px;
color: ${theme.colors.primary};
`

export const TableHeader = styled.View`
align-items: center;
background-color: #F3F3F3;
padding: 13px 0;
`

export const TableHeaderText = styled(Text)`
font-size: 16px;
`
