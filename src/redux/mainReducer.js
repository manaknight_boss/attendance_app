import { 
    SET_BEARER_TOKEN, 
    REMOVE_ACCESS_TOKEN,
    ADD_EMAIL,
    ADD_PASSWORD ,
    SET_PROFILE,
} from './constants'

//grassroot2@manaknight.com
//a123456
const DEFAULT_STATE = {
    bearerToken: "",
    profile: null,
    email: "grassroot@manaknight.com",
    password: "a123456",
}

const DEFAULT_ACTION = {
    type: 'DEFAULT_ACTION',
}

export default function (
    state = DEFAULT_STATE,
    action = DEFAULT_ACTION 
) {
    const {type, payload} = action
    console.log(type, '=>', payload, '\n')
    switch (type) {
        case SET_BEARER_TOKEN:
            return {
                ...state,
                bearerToken: payload
            }
        case SET_PROFILE:
            return {
                ...state,
                profile: payload
            }
        default:
            return state;
    } }
