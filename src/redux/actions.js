import { 
    SET_BEARER_TOKEN ,
    SET_PROFILE,
} from './constants'

export function setBearerToken(bearerToken) {
    return {
        type: SET_BEARER_TOKEN,
        payload: bearerToken
    }
}

export function setProfile(payload) {
    return {
        type: SET_PROFILE,
        payload
    }
}
