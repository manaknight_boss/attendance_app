import { Provider as PaperProvider } from 'react-native-paper';
import { Platform } from 'react-native';
import React, { Component } from 'react';

import { theme } from './src/theme';
import NavigationContainer from './src/navigation/NavigationContainer';
import { Provider } from 'react-redux'
import store from './src/redux/store'

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' + 'Shake or press menu button for dev menu',
});


export default class App extends Component {
  render() {
    return (
        <Provider store={store}>
            <PaperProvider theme={theme}>
                <NavigationContainer/>
            </PaperProvider>
        </Provider>
    );
  }
}

